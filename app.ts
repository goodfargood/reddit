import  { 
	Component,
	NgModule 
} from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";

class Article {
	points: number;
	title: string;
	link: string;
	constructor (points : number , title : string , link : string) {
		this.points = points || 0;
		this.title = title ;
		this.link = link ;
	}
	voteUp() {
		this.points += 1;
	}
	voteDown() {
		this.points -= 1;
	}
	domain() : string {
		try {
			const link: string = this.link.split("//")[1];
			return link.split("/")[0];
		} catch(err) {
			return null;
		}

	}
}

@Component({
	selector: "reddit-creator",
	template: `
	<div class="col-lg-10  " style="padding-top:1%;    float: none; margin: 0 auto;">
	<form>
		<div class="form-group" >
			<label for="title" > title: </label>
			<input type="text" class="form-control" #title />
		</div>
		<div class="form-group">
			<label for="link"> link:  </label>
			<input type="text" class="form-control" #link />
		</div>
			<div class="form-group">
			<input value="send!" type="button" class="btn btn-primary" (click)="addArticle(title,link)" />
		</div>
	</form> 
	<div>
	<redit-article *ngFor="let article of sortedArticles()"  [article]="article"></redit-article>
	</div>
	</div>`

})
class RedditCreator {
	articles: Array<Article>;
	constructor() {
		this.articles = [
			new Article( 0 , 'angular2' , 'http://goodarzif.ir' ),
			new Article( 5 , 'ang2' , 'http://goodfargood.com/lessthanmore' )
			];
	}
	addArticle(title: HTMLInputElement , link: HTMLInputElement) {
		console.log(`this is title:  ${title.value}  and this is link:  ${link.value}`);
		this.articles.push(new Article(0 , title.value , link.value));
		title.value = '';
		link.value = '';
	}
	sortedArticles(): Article[] {
		return this.articles.sort((a: Article, b: Article) => b.points - a.points);
	}
}

@Component({
	selector: "redit-article",
	host: {
		class: 'row'
	},
	inputs: [ 'article' ],
	template: `
		<div class="row" style="float:none;margin:0 auto; width:80%;">
			<div class="col-lg-4" style="float:left;">
				<h1 style="padding:30%;background-color:gray;">{{ article.points }} </h1>
			</div>
			<div class="col-lg-6" style="float:right;">
				<div class="row">
					<h1 >{{ article.title }}</h1>
				</div>
				<div class="row">
					<a href="{{ article.link }}"> {{ article.domain() }}</a>
				</div>
				<div class="row">
					<button class="btn btn-danger" (click)="voteUp()">vote up</button>
					<button class="btn btn-success" (click)="voteDown()"> vote down </button>
				</div>
			</div>
		</div> `
})
class RedditArticle {
	article: Article;
	constructor() {}
	voteUp(): boolean {
		this.article.voteUp();
		return false;
	}
	voteDown(): boolean {
		this.article.voteDown();
		return false;
	}

}


@NgModule({
	declarations: [
		RedditCreator , RedditArticle  ] ,
	imports: [ BrowserModule ] ,
	bootstrap: [ RedditCreator ]
})
class RedditAppModule {}
platformBrowserDynamic().bootstrapModule(RedditAppModule);
